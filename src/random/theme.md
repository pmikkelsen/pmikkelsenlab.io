{title: Test page to view theme}
{date: 2020/05/28}

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

Normal paragraph

**Bold text**

*Italic text*

Here is some `inline code` which should look nice.

Below is an entire code block
```
-module(test).
-export([hello/1]).

hello(Name) ->
	io:format("Hello to ~s!~n", [Name]).
```

- Here
- Is
- A
- List

1. Here
2. Is
3. Another
4. List

<header>Here is a header</header>

<footer>Here is a footer</footer>

> "Here is a block quote"
>
> -- me, 2020

Here is [a link to something](https://pmikkelsen.com)

And finally, below is an image
[![Test image][1]][1]

[1]: %%base-path%%/images/acme-in-action.png