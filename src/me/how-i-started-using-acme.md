{title: How I started using acme}
{date: 2020/02/26}

## The beginning
My very first code editor was visual studio 2010 for windows 7. This was
when I didn't know what programming was, and a teacher I had suggested
that we used that, so we could start learning some C#. It was very fun and
all, but soon after, I decided that I wanted to learn another language,
and the whole idea of using an IDE which is specialized for a specific
programming language or environment just seemed very odd to me. Surely
there had to be better options, where I as a user could decide by myself
what languages I wanted support for. I tried out multiple different
small editors but didn't really like any of them.

After some time, I decided that I wanted to try linux, and I installed
fedora or linux mint (don't remember at this point), and I liked it very
much. Comming from windows where it is normal just to go to a random
website and download the tools I needed, it was a great joy to be able
to just type a command in the terminal and watch my program get installed.

## First try: vim
After reading online for some time, it seemed that the editor which
all the cool linux users used was vim, so I decided to learn that. The
experience was very different from what I was used to, but I liked the
keyboard shortcuts and that it looked very cool. Of course I installed
every plugin that I had read about online and it all became a mess,
but I stuck with it for around 3-4 years. The things I liked the most
was that I felt it was very fast to do what I wanted, but unfortunately
the flow with multiple files was never something I could get used to,
and all the plugins made me sick.

## Second try: emacs
Just to give another very popular editor a try, I installed emacs sometime
2-3 years ago. Comming from vim, the keyboard shortcuts in emacs sucks,
but the editor itself is just so much more powerful, because plugins and
customisations could be written in a real programming language. I started,
as many other emacs users do, to use emacs for everything i could. I read
my mails, watched PDFs, managed my system, and even used it as my window
manager for a period. Around the same time that I started using emacs,
I also started using OpenBSD as a secondary operating system. I noticed
that many very good tools are available in unix, but emacs almost has
everything implemented again in elisp, which seemed stupid to me. But hey,
it works so I stuck with it. Until late 2019.

## Now acme
One day in school when one of my group mates asked if I could look at
something for him, I saw that he was using visual studio code, which
made it natural to use a mouse to click around in the file. Of course
this was also possible in emacs, but since I came from vim, it was never
something that I did. He argued that it was much faster to just click in
the file where he wanted to edit, than to navigate using the keyboard, and
I thought that maybe he was right, so I started digging. After some time
looking though the internet for some editors which made much use of the
mouse, I found acme and decided to install it. And wow it was different.

[![A picture of acme in action][1]][1]

Acme uses the mouse for everything, since there are very few keyboard
shortcuts (not even copy and paste). As shown on the picture, there are no
menus and no icons; everything is just text, and the different buttons on
the mouse can interpret that text in different ways. For example, middle
clicking on the text `New` wil create a new text frame, and right clicking
on the text `example` will search the file for the word. Actually, it is
more advanced than that since the right click will first send the selected
text to something called the plumber, which might do something fun like
opening the file which has that name. I suggest interrested readers to
read the page [here](http://acme.cat-v.org/) and some of the documents
linked from that site to learn more about acme. The video introduction
from Russ Cox [here](https://www.youtube.com/watch?v=dP1xVpMPn8M) is
also great.


## What I like about acme
* **The colours**: they cannot be changed without modifying the source,
but luckily for me, I love those pale bright colours. In fact, there
are the very reason this website has the colours that it has.

* **Writing commands**: since acme allows users to control it via the 9P
protocol (if you don't understand, read the links above), it is possible
to write "plugins" or commands in whatever language I want. After
just creating a program with the needed functionality, it is possible
to write the name somewhere and middle click on it, just like it was
always there. Not may editors makes it possible to use the environment
as much as acme does, which was also one of my wonders about emacs back
then. It allows me to turn it into *my* IDE using whatever tools I see fit,
instead of depending on support directly in the editor.

* **Everything as text**: this means that if I run a gdb session via
the `win` command, it is totally possible for me to just scroll up in the
history and delete lines which are not important, and to write notes
as I debug. Also, since commands are just text which is clicked, it is
possible to have a document with commands which are useful in a given
project, which I can then open and click.

* **Lack of customisation**: while this might seem strange, comming from
a vim and emacs background, it surely is wonderful to have an editor
which does *not* encourage the user to customise the hell out of it. I
have spent way too much time doing this in the past and it had to stop.

* **Lack of syntax highlighting and auto completion**: some people love
it and can't live without it, but I very much prefer to learn the syntax
of the language based on the contents, not based on some colours an
editor throws at me. Also I feel like I learn and remember much better
when there is no auto completion.

## Other editors I use sometimes
Sometimes I have to do very small editing tasks, and sometimes as root,
in which case it is just simpler to open the file in vi. Note that this
is the small vi, and not the "improved" vim. I also sometimes edit my
text in sam with is another editor written by Rob Pike, and I quite like
that one too for smaller projects. You can read more about sam
[here](http://sam.cat-v.org/).

[1]: %%base-path%%/images/acme-in-action.png