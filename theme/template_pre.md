<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>%%title%%</title>
	<link rel="stylesheet" href="%%base-path%%%%stylesheet%%">
</head>
<body>
<header id="nav_header" class="acmelike-bar">

- [Home](%%base-path%%/index.html)
- [About](%%base-path%%/about.html)
- [Contact](%%base-path%%/contact.html)
- [Gitlab](https://gitlab.com/pmikkelsen)
- [Sourcehut](https://git.sr.ht/~pmikkelsen)

</header>
<header id="page_header" class="acmelike-bar">
<div id="blue_box"></div>

- %%page-location%%

</header>
<main>

<div id=page_info>

*%%date%%*

# %%title%%

</div>



