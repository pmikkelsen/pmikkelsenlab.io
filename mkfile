MKSHELL=$PLAN9/bin/rc

content_pages=`{ls src/* | grep '\.md$' | grep -v 'src/(index|about|contact|404).md'}
extra_pages=src/about.md src/contact.md src/404.md src/index.md
all_pages=$content_pages $extra_pages

varpages=${all_pages:%.md=%.varpage}
html=${varpages:src/%.varpage=public/%.html}

static_files=`{ls src/* | grep '\.(png)|(css)$' | sed 's,^src,public,g'}

all:V: $html $static_files

public/%.png:Q: src/%.png
	echo Copying image to $target
	mkdir -p `{basename -d $target}
	cp $prereq $target

public/%.css:Q: src/%.css
	echo Copying CSS to $target
	mkdir -p `{basename -d $target}
	cp $prereq $target

src/index.md:Q: theme/index.md $content_pages
	echo Updating $target
	rm -f $target
	for (page in $content_pages) {
		page_date=`{rc ./findvar.rc date $page}
		echo $page_date $page >> $target
	}
	pages=`{sort -r $target | sed 's/^[^ ]+ //g'}
	cat theme/index.md > $target
	for (page in $pages) {
		page_title=`{rc ./findvar.rc title $page}
		page_date=`{rc ./findvar.rc date $page}
		output=`{echo $page | sed -e 's,^src,,g' -e 's,.md$,.html,g'}
		echo - $page_date - [$"page_title]'(%%base-path%%'$output')' >> $target
	}

public/%.html:Q: src/%.varpage src/%.md
	echo Replacing vars in $target
	mkdir -p `{basename -d $target}
	source_file=src/$stem.md
	var_file=src/$stem.varpage
	variables=`{ssam -n 'x/%%[^%]+%%/' $var_file \
		| sed -e 's,%%%%,%%\n%%,g' -e 's,%,,g' | sort | uniq}
	ifs='' {
		content=`{cat $var_file}
	}
	for (name in $variables) {
		value=`{rc ./findvar.rc $name $source_file}
		if (~ $#value 0) {
			switch ($name) {
			case base-path
				value=$base
			case stylesheet
				value=/style.css
			case page-location
				value=`{echo $target | sed 's,^public,,g'}
			case source-location
				value=$source_file
			case repository
				value='https://gitlab.com/pmikkelsen/pmikkelsen.gitlab.io'
			case date
				value=''
			case *
				value=UNDEFINED
			}
		}
		ifs='' {
			content=`{echo $content | sed 's,%%'$name'%%,'$"value',g'}
		}
	}
	echo $content > $target

%.varpage:Q: %.md theme/template_pre.md theme/template_post.md
	echo Adding template to $stem.md
	mkdir -p `{basename -d $target}
	cat theme/template_pre.md $stem.md theme/template_post.md \
		| sed -e 's,^{.+}$,,g' \
		| cmark -t html --unsafe --smart > $target

clean:V:
	rm -rf public tmp $varpages src/index.md
