{title: About}

## This website
My intention with this website it to share some ideas and thoughts on
programming and other computer related subjects. The things I care
about and find interesting at the moment are written below, and are
updated sometimes. Hopefully, most of the content will be about one of
those things.

- Functional programming (Haskell, Erlang, Scheme)
- Concurrent programming (Erlang, Go)
- Plan 9 (both the 9front fork and plan9port)

Things I don't care much about and therefore will most likely never
write about (except possible complaints) includes

- The modern web
- Object oriented programming
- Proprietary software

For information about how the website it setup, see
[this](%%base-path%%/setup-of-this-site.html) post.

## Me
My name is Peter Mikkelsen and I am a computer science student at Aalborg University, Denmark.

The following list contains other facts and links
- **Age**: 21
- **Favourite colours**: Pale bright colors, like those on this site
- **Favourite programming language**: See [this](%%base-path%%/opinions/favourite-programming-languages.html) post
- **Current job**: None :)
- **Stuff I use**: See [this](%%base-path%%/me/stuff-i-use.html) post.
- **Tabs vs spaces**: Tabs.
- **Emacs** *vs* **Vi** *vs* **Nano**: [None of them](%%base-path%%/me/how-i-started-using-acme.html)