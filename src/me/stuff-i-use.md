{title: Stuff I use}
{date: 2020/05/28}

* Note: Last updated on 2020/05/28

## Operating system
I mostly use [guix][1]  but sometimes I need software that is not yet
ported, and then I use [fedora][2]. If I could, I would use [9front][3]
alot more, since the system is much better and different in my opinion. I
also like [OpenBSD][4] alot.

## Text editor
Acme from plan9port. Sometimes I use emacs if I have to edit scheme code, since automatic closing of matching `(` and `)` makes life much easier.

## Shell
I use the [rc shell][5] since it works nicely in both acme and 9term from plan9port. Also I like its syntax alot more than the syntax of bash.

## Web browser
Mainy firefox (this page is only tested on firefox, so please let me
know if you have problems on other browsers).

## Laptop
I use a Lenovo Thinkpad E495 with an AMD ryzen 3700U and 16 gigabytes
of ram. It is a fairly good computer for the price, and OpenBSD is
supported out of the box with the exception of wifi. For this reason, I
have small usb wifi dongle that is constantly plugged in, which performs a
lot worse than what some people would like, but for me it is no big deal.


## Mouse and keyboard
Since I have never been a fan of touchpads on laptops, I use an external
mouse which is a Logitech MX Master 3. I also have a keyboard that I
sometimes plug in if I have to do more writing than what I can comfortably
do on my laptop. The keyboard is from the coolermaster masterkeys lite L
bundle. Clicking in acme using the scroll wheel instead of using a true
3-button mouse is sometimes a bit annoying, but its OK.


[1]: https://guix.gnu.org/
[2]: https://getfedora.org/
[3]: http://9front.org/
[4]: https://openbsd.org/
[5]: https://plan9.io/sys/doc/rc.html